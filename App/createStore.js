import { createStore, applyMiddleware, compose } from 'redux';
import reducers from './reducers';

const middleware = [/* ...your middleware (i.e. thunk) */];
const store = compose(
	applyMiddleware(...middleware)
)(createStore)(reducers);

export default store;