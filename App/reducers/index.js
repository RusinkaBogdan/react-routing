import { combineReducers } from 'redux';
import counter from './counter';
import routes from './routes';
// ... other reducers

export default combineReducers({
  counter,
  routes
  // ... other reducers
});