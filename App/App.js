import React, { Component } from 'react';
import { Router, Scene, ActionConst, Actions } from 'react-native-router-flux';
import { connect, Provider } from 'react-redux';
import {
  Text,
  View,
  TouchableOpacity,
  Image,
  Navigator,
  TouchableHighlight
} from 'react-native';

import {
  StackNavigator,
  DrawerNavigator
} from 'react-navigation';

import Objectives from './components/Objectives';
import Home from './components/Home';
import User from './components/User';
import Launch from './components/Launch';
import TabView from './components/TabView';
import NavigationDrawer from './components/NavigationDrawer';
import TabIcon from './components/TabIcon';
import KeyResult from './components/KeyResult';
import styles from './styles/root';
import store from './createStore';
import getSceneStyle from './util/utils';

const RouterWithRedux = connect()(Router);

class OKRapp extends Component {
	render(){
		const routes = [
			{title: 'First Scene', index: 0},
			{title: 'Second Scene', index: 1, data: {}},
		];
		return (
			<Navigator
			  initialRoute={routes[0]}
			  initialRouteStack={routes}
			  renderScene={(route, navigator) =>{
			  	switch(route.index){
					case 0:
						return <Objectives navigator={navigator} />
						break;
					case 1:
						return <KeyResult data={route.data} />
						break;
				}
			  }}
			  navigationBar={
				 <Navigator.NavigationBar
				   routeMapper={{
					 LeftButton: (route, navigator, index, navState) =>
					 {
					    if (route.index === 0) {
					      return null;
					    } else {
					      return (
					        <TouchableHighlight onPress={() => navigator.pop()}>
					          <Text>Back</Text>
					        </TouchableHighlight>
					      )
					    }
					},
					 RightButton: (route, navigator, index, navState) =>
					   { return (<Text>Done</Text>); },
					 Title: (route, navigator, index, navState) =>
					   { return (<Text>Awesome Nav Bar</Text>); },
				   }}
				   style={{backgroundColor: 'gray'}}
				 />
			  }
			/>
		  );
	}
}

export default OKRapp