import React, {Component} from 'react';
import {PropTypes} from "react";
import {StyleSheet, Text, View } from "react-native";
import { Actions } from 'react-native-router-flux';
import Button from 'react-native-button';

const contextTypes = {
  drawer: React.PropTypes.object,
};

const propTypes = {
  name: PropTypes.string,
  sceneStyle: View.propTypes.style,
  title: PropTypes.string,
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
  	backgroundColor: 'darkslateblue',
  },
  button: {
  	margin: 5,
  	left: 10,
  	height: 40,
  	color: 'white',
  	alignSelf: 'stretch',
  	width: 360,
  	textAlign: 'left'
  }
});

class TabView extends Component {
  constructor(){
  	super()
  }

  closeDrawer(){
  	Actions.refresh({key: 'drawer', open: value => false });
  	Actions.tab1_1();
  }

  render(){

	  return (
	    <View style={[styles.container]}>
	      <Button style={styles.button} onPress={this.closeDrawer}>Tab1_1</Button>
	    </View>
	  );
	 }
};

export default TabView;