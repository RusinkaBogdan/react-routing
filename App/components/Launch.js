import React, {Component} from 'react';
import {View, Text, StyleSheet} from "react-native";
import { Button } from "react-native";
import {Actions} from "react-native-router-flux";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "transparent",
    borderWidth: 2,
    borderColor: 'red',
  }
});

export default  class Launch extends Component {
  goHome(){
    Actions.home({title: 'Home'})
  // Redux require you to return an object with type
    return {type: 'CardPush'}
  }

  render(){
    return (
      <View style={styles.container}>
        <Text>Launch page</Text>
        <Button onPress={this.goHome.bind(this) } title='go'>Go to Login page</Button>
        <Button onPress={Actions.pop} title='fuck'>back</Button>
      </View>
    );
  }
}
