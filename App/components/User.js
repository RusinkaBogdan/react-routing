import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import Counter from '../components/Counter';
import * as counterActions from '../actions/counterActions';
import { connect } from 'react-redux';
import { View, Text, Button } from 'react-native';
import {Actions} from "react-native-router-flux";

export default class Home extends Component {

	constructor(props){
		super(props);
	}

	openDrawer(){
		Actions.refresh({key: 'drawer', open: true })
	}

	render() {
		return (
			<View>
				<Text>User</Text>
				<Button onPress={Actions.pop} title='Back'>Back</Button>
				<Button onPress={this.openDrawer.bind(this)} title='Open Drawer'></Button>
			</View>
		);
	}

}