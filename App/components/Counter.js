import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity
} from 'react-native';

export default class User extends Component {

	constructor(props){
		super(props);
	}

	

	render() {
		return (
			<View>
				<Text>{this.props.counter}</Text>
				<TouchableOpacity onPress={this.props.increment} style={styles.button}>
							<Text>up</Text>
				</TouchableOpacity>
				<TouchableOpacity onPress={this.props.decrement} style={styles.button}>
						<Text>down</Text>
				</TouchableOpacity>
			</View>
		);
	}

}

const styles = StyleSheet.create({
  button: {
	width: 100,
	height: 30,
	padding: 10,
	backgroundColor: 'lightgray',
	alignItems: 'center',
	justifyContent: 'center',
	margin: 3
  }
});