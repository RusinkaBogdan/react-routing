import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Image,
  ScrollView,
  Alert
} from 'react-native';

export default class KeyResult extends Component {
  
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <View style={styles.container}>
        <Image style={styles.button} source={{uri: this.props.data.icon_url}} />
          <Text style={styles.header}>{this.props.data.name}</Text>
          <Text style={styles.instructions}>{this.props.data.url}</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  header: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  button: {
    width: 200,
    height: 200
  },
  scrollView: {
    backgroundColor: '#6A85B1',
    height: 300,
  },
});