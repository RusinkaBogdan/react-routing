import React, { Component } from 'react';
import {
	AppRegistry,
	StyleSheet,
	Text,
	View,
	Image,
	ScrollView,
	Alert,
	TouchableOpacity,
	ActivityIndicator,
	PixelRatio,
	DrawerLayoutAndroid
} from 'react-native';

import Button from 'react-native-button';
import { Actions } from 'react-native-router-flux';

export default class scrollView extends Component {
	
	constructor(props) {
		super(props);
		this.state = {
			albums: [],
			animating: true
		};
		this.getAlbumsFromApiAsync();
	}

	navigationView(){
    return (<View style={{flex: 1, backgroundColor: '#fff'}}>
      <Text style={{margin: 10, fontSize: 15, textAlign: 'left', top: 100}}>I'm in the Drawer!</Text>
    </View>)
  	}

	getAlbumsFromApiAsync() {
		return fetch('https://teamtreehouse.com/matthew.json')
			.then((response) => response.json())
			.then((responseJson) => {
				console.log(responseJson)
				this.setState({albums: responseJson.badges.splice(0,5)})
			})
			.catch((error) => {
				console.error(error);
			});
	}

	render() {
		return (
			<DrawerLayoutAndroid
      drawerWidth={300}
      drawerPosition={DrawerLayoutAndroid.positions.Left}
      renderNavigationView={() => this.navigationView() }>
			<ScrollView style={styles.scrollView}>
			 { 
				!this.state.albums.length ? <ActivityIndicator
					animating={this.state.animating}
					style={[styles.container, {height: 130}]}
					size="large"
				/> : null
			}
			{
				this.state.albums.map((item, index)=>{
					return (
						<View key={index}>
						<TouchableOpacity style={styles.row} onPress={()=>this.props.navigator.push({index: 1, data: item}) }>
							<Image key={index} style={styles.image} source={{uri: item.icon_url}} />
							<View style={styles.data}>
								<Text style={styles.itemHeader}>{item.name}</Text>
								<Text>{item.url}</Text>
							</View>
						</TouchableOpacity>
						</View>
					)
				})
			}
			</ScrollView>
			</DrawerLayoutAndroid>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
	},
	itemHeader: {
		fontSize: 20
	},
	icon: {
		width: 20,
		height: 20
	},
	container: {
		alignItems: 'center',
		justifyContent: 'center',
		top: 50
	},
	row: {
		flexDirection: 'row',
		borderColor: '#efefef',
		borderBottomWidth: 1 / PixelRatio.get(),
		borderWidth: 1,
	},
	data: {
		marginBottom: 5,
		marginLeft: 10,
		flex: 1
	},
	image: {
		width: 40,
		height: 40,
		margin: 10
	},
	scrollView: {
		backgroundColor: '#fff',
		height: 700,
		flex: 1,
		width: 360
	},
});