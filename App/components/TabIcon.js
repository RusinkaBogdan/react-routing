import React, {
  PropTypes,
} from 'react';
import {
  Text,
} from 'react-native';


const TabIcon = (props) => (
  <Text
    style={{ color: props.selected ? 'red' : 'black' }}
  >
    {props.title}
  </Text>
);

export default TabIcon;