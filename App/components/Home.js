import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import Counter from '../components/Counter';
import * as counterActions from '../actions/counterActions';
import { connect } from 'react-redux';
import { View, Button } from 'react-native';
import {Actions} from "react-native-router-flux";

class Home extends Component {

	constructor(props){
		super(props);
	}

	back(){
		Actions.pop();
		Actions.refresh();
	}

	goToUser(){
		Actions.user({title: 'User'})
  // Redux require you to return an object with type
    return {type: 'CardPush'}
	}

		tabBar(){
		Actions.tabbar({title: 'User'})
  // Redux require you to return an object with type
    return {type: 'CardPush'}
	}

	render() {
		return (
			<View>
				<Counter
        counter={this.props.counter}
        {...this.props.actions } 
        />
        <Button onPress={this.back.bind(this)} title='Back'></Button>
        <Button onPress={this.goToUser.bind(this)} title='User'></Button>
        <Button onPress={this.tabBar.bind(this)} title='Bar'></Button>
			</View>
		);
	}

}

export default connect(state => ({
	counter: state.counter.count,
	routes: state.routes
}),

 (dispatch) => ({
    actions: bindActionCreators(counterActions, dispatch)
  })
)(Home);