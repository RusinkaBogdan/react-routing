import {
  AppRegistry
} from 'react-native';

import OKRapp from './App/App'

AppRegistry.registerComponent('OKRapp', () => OKRapp);
